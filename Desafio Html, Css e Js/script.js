
let n1 = "0";
let n2 = ""
let operacao = null;
let clicadoEmIgual = false;
let primeiroDigitoLimite = false;
let segundoDigitoLimite = false;

function incluirDigito(digito) {

  if (clicadoEmIgual) {
    clicadoEmIgual = false;
    n1 = digito;
    mostrarNoDisplay(n1);
    return
  }

  if (operacao !== null) {
    segundoDigitoLimite = true;
    verificaLimiteDigitos(n2);
    n2 += digito;
    segundoDigitoLimite = false;
    mostrarNoDisplay(n2);
  } else {
    if (n1 === "0") {
      n1 = digito;
    } else {
      primeiroDigitoLimite = true;
      verificaLimiteDigitos(n1)
      n1 += digito;//concatena
      primeiroDigitoLimite = false;
    }
    mostrarNoDisplay(n1)
  }
}

function finalizarCalculo() {
  clicadoEmIgual = true;
  let resultado = calcular();
  n1 = resultado;
  mostrarNoDisplay(n1);
  console.log("n1", n1, "operacao", operacao, "n2", n2)
  operacao = null;
  n2 = ""
}

function obterPorcento() {
  if (!n2) {
    n2 = "";
    mostrarNoDisplay(n1);
  } else {
    let porcento = n1 * n2 / 100;
    n2 = porcento;

    mostrarNoDisplay(n2)
  }
}

function calcular() {
  let nCalculado = 0;
  let _n1 = parseFloat(n1)
  let _n2 = parseFloat(n2)

  switch (operacao) {
    case "+":
      nCalculado = _n1 + _n2;
      break;
    case "-":
      nCalculado = _n1 - _n2;
      break;
    case "*":
      nCalculado = _n1 * _n2;
      break;
    case "/":
      nCalculado = _n1 / _n2;
      break;
  }
  if (nCalculado > 999999999) {
    alert("O limite maximo visivel pelo display foi atingido, resultado da operacao: " + nCalculado)
    document.location.reload(true);
  }
  return nCalculado;
}

function iniciarCalculo(simbolo) {
  clicadoEmIgual = false;
  if (operacao === null || n2 === "") {
    operacao = simbolo;
  } else {
    //ja tem n1, operacao e n2
    let resultado = calcular();
    n1 = resultado;
    operacao = simbolo;
    n2 = "";
    mostrarNoDisplay(n1)
  }
}

function incluirPonto() {
  if (operacao && n2 === "") {
    n2 = "0.";
  } else if (operacao && n2) {
    n2 += ".";
  } else {
    n1 += ".";
  }
}

function limpar() {
  n1 = "0";
  operacao = null;
  n2 = "";
  mostrarNoDisplay(n1);
}

function mostrarNoDisplay(valor) {
  document.querySelector("#resultado").innerHTML = valor;
}

function verificaLimiteDigitos(numero) {
  if (numero > 999999999) {

    if (primeiroDigitoLimite) {
      console.log(" 1 Limite ultrapassado!")
      n1 = ""
    }

    if (segundoDigitoLimite) {
      console.log(" 2 Limite ultrapassado!")
      n2 = ""
    }

  }
}